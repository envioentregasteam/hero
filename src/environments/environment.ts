import authInfo from '../../auth_config.json';

export const environment = {
  production: false,
  api_local: 'http://localhost:8253',
  api_stg: 'https://us-central1-hero-2022.cloudfunctions.net/app',
  auth: {
    domain: authInfo.authInfo.domain,
    clientId: authInfo.authInfo.clientId,
    redirectUri: window.location.origin,
    audience: authInfo.authInfo.audience
  },
  dev: {
    serverUrl: authInfo.authInfo.serverUrl
  }
};