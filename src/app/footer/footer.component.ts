import { Component, OnInit } from '@angular/core';
import { SpinnerService } from '../shared/services/spinner.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  currentYear: string;
  email = 'mauriciomage@gmail.com';
  isLoading$ = this.spinnerService.isLoading$;

  constructor(private spinnerService: SpinnerService) { }

  ngOnInit(): void {
    this.currentYear = new Date().getFullYear().toString();
  }

}
