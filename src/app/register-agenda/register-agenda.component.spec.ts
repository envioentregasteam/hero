import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RegisterAgendaComponent } from './register-agenda.component';

describe('RegisterAgendaComponent', () => {
  let component: RegisterAgendaComponent;
  let fixture: ComponentFixture<RegisterAgendaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterAgendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
