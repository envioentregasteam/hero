import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  visible$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private http: HttpClient
  ) { }

  getAllUsers(token: string): Observable<any> {
    return this.http.get(`/api/users`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  }
}
