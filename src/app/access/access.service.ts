import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccessService {

  constructor(private http: HttpClient) { }

  public getExpenses(): Observable<any> {
    return this.http.get(`${environment.api_stg}/api/expense/get`, {});    
  }

}
