import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavbarService } from '../navbar/navbar.service';
import { AccessService } from './access.service';

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.scss']
})
export class AccessComponent implements OnInit {

  expenses: [] = [];  

  constructor(
    private route: Router,
    private navbar: NavbarService,
    private accessService: AccessService
  ) { }

  ngOnInit(): void {
    this.navbar.visible$.next(true);
    this.accessService.getExpenses().subscribe((response) => {
      this.expenses = response.data;
    })
  }

}
