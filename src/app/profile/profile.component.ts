import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { NavbarService } from '../navbar/navbar.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profile: string = null;

  constructor(
    public auth: AuthService,
    public navbarService: NavbarService
  ) { }

  ngOnInit(): void {
    this.navbarService.visible$.next(true);
    this.auth.user$.subscribe((profile) => (this.profile = JSON.stringify(profile, null, 2)));
  }

}
