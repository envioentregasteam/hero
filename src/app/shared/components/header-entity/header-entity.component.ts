import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-entity',
  templateUrl: './header-entity.component.html',
  styleUrls: ['./header-entity.component.scss']
})


export class HeaderEntityComponent implements OnInit {
  
  @Input() data: any[] = [];
  @Input() title: string; 
  @Input() newEntityUrl: string; 
  @Input() newEntityButtonText: string;
  
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  navigate() {
    this.router.navigate([this.newEntityUrl]);
  }
}
