import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderEntityComponent } from '../shared/components/header-entity/header-entity.component';
import { LoaderComponent } from '../shared/components/loader/loader.component';
import { MatDatepickerModule } from '@angular/material/datepicker';



@NgModule({
  declarations: [
    HeaderEntityComponent,
    LoaderComponent
  ],
  exports: [
    HeaderEntityComponent
  ],
  imports: [
    CommonModule,
    MatDatepickerModule
  ],
  entryComponents: [
    HeaderEntityComponent
  ]
})
export class SharedModule { }
