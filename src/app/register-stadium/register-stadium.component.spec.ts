import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RegisterStadiumComponent } from './register-stadium.component';

describe('RegisterStadiumComponent', () => {
  let component: RegisterStadiumComponent;
  let fixture: ComponentFixture<RegisterStadiumComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterStadiumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterStadiumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
