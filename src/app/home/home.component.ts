import { Component, OnInit } from '@angular/core';
import { AgendaService } from '../agenda/agenda.service';
import { Agenda } from '../shared/models/agenda.model';
import { SHOW_MAX_AGENDA } from '../shared/constants/general';
import { AuthService } from '@auth0/auth0-angular';
import { NavbarService } from '../navbar/navbar.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title = 'Hero Admin';
  isAuthorized: boolean;
  username: string;
  isErrorConnection: boolean;
  errors: any[];
  agenda: Agenda[];
  showMaxAgenda: number = SHOW_MAX_AGENDA.limit;

  constructor(
    private agendaService: AgendaService,
    public auth: AuthService,
    private navbarService: NavbarService
  ) { }

  ngOnInit() {
    this.navbarService.visible$.next(false);
    this.errors = [];    
  }
}
