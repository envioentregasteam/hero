import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { Router } from '@angular/router';
import { NavbarService } from './navbar/navbar.service';
import { SpinnerService } from './shared/services/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  username: string;
  isAuthorized: boolean;
  visible: boolean;

  constructor(
    public auth: AuthService,
    public navbarService: NavbarService
  ) {}

  ngOnInit(): void {
  }
}
