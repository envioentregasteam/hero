import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RegisterAwardsComponent } from './register-awards.component';

describe('RegisterAwardsComponent', () => {
  let component: RegisterAwardsComponent;
  let fixture: ComponentFixture<RegisterAwardsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterAwardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterAwardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
