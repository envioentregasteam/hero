import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from '@auth0/auth0-angular';
import { NotfoundComponent } from './notfound/notfound.component';
import { RegisterUsersComponent } from './register-users/register-users.component';
import { UsersComponent } from './users/users.component';
import { AgendaComponent } from './agenda/agenda.component';
import { RegisterAgendaComponent } from './register-agenda/register-agenda.component';
import { StadiumsComponent } from './stadiums/stadiums.component';
import { RegisterStadiumComponent } from './register-stadium/register-stadium.component';
import { AwardsComponent } from './awards/awards.component';
import { RegisterAwardsComponent } from './register-awards/register-awards.component';
import { HistoricalComponent } from './historical/historical.component';
import { UsersAwardsComponent } from './users-awards/users-awards.component';
import { ProfileComponent } from './profile/profile.component';
import { AccessComponent } from './access/access.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'access', component: AccessComponent, canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'users', component: UsersComponent, canActivate: [AuthGuard]},
  {path: 'agenda', component: AgendaComponent, canActivate: [AuthGuard]},
  {path: 'stadiums', component: StadiumsComponent, canActivate: [AuthGuard]},
  {path: 'awards', component: AwardsComponent, canActivate: [AuthGuard]},
  {path: 'historical', component: HistoricalComponent, canActivate: [AuthGuard]},
  {path: 'users-awards', component: UsersAwardsComponent, canActivate: [AuthGuard]},

  {path: 'register-user', component: RegisterUsersComponent, canActivate: [AuthGuard]},
  {path: 'register-agenda', component: RegisterAgendaComponent, canActivate: [AuthGuard]},
  {path: 'register-stadium', component: RegisterStadiumComponent, canActivate: [AuthGuard]},
  {path: 'register-awards', component: RegisterAwardsComponent, canActivate: [AuthGuard]},

  {path: '**', component: NotfoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
