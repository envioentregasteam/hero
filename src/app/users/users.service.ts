import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { User } from '../shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient
  ) { }

  getAllUsers(token: string): Observable<any> {
    return this.http.get(`/api/users`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  }

  edit(user: User, token: string): Observable<any> {
    return this.http.post(`${environment.api_local}/api/edit/user`, {
        user
    }, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  }

  delete(user: User): Observable<any> {
    return this.http.post(`${environment.api_local}/api/delete/user`, {
        user
    });
  }
}
