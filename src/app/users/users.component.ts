import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { User } from '../shared/models/user.model';
import { EditUserModalComponent } from './edit-user-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(
    private usersService: UsersService,
    private modalService: NgbModal,
    private router: Router,
    private authService: AuthService
  ) {}

  allowPopupModal: any;
  username: string;
  users: User[];
  errors: any[];
  isErrorConnection: boolean;
  token: string;

  ngOnInit() {
    this.errors = [];
    this.users = [];
    this.isErrorConnection = false;
    this.authService.getAccessTokenSilently().subscribe(token => {
      if (token) {
        this.token = token;
        this.getUsers();
      } else {
        this.isErrorConnection = true;
      }
    });
  }

  editUser(user) {
    this.allowPopupModal = this.modalService.open(EditUserModalComponent);
    this.allowPopupModal.componentInstance.user = user;
  }

  deleteUser(user) {
    // this.allowPopupModal = this.modalService.open(DeleteUserModalComponent);
    // this.allowPopupModal.componentInstance.user = user;
  }

  public navigate(page: string): void {
    this.router.navigate([page]);
  }

  private getUsers() {
    this.usersService.getAllUsers(this.token).subscribe(
      (users) => {
        this.isErrorConnection = false;
        this.users = users;
      },
      err => {
        this.isErrorConnection = true;
        this.errors.push('Ha ocurrido un error en la conexión, contactese con el Admin.');
    });
  }
}
