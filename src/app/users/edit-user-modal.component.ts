import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UsersService } from './users.service';
import { User } from '../shared/models/user.model';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-edit-user-modal',
  templateUrl: './edit-user-modal.component.html',
  styleUrls: ['./edit-user-modal.component.scss']
})
export class EditUserModalComponent implements OnInit {

  isErrorConnection: boolean;
  isUserEdited: boolean;
  isSomeFieldEmpty: boolean;
  errors: any[];
  token: string;
  @Input() user: User;

  constructor(
    public modalReference: NgbActiveModal,
    private userService: UsersService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.isErrorConnection = false;
    this.isUserEdited = false;
    this.isSomeFieldEmpty = false;
    this.errors = [];
    this.authService.getAccessTokenSilently().subscribe(token => {
      if (token) {
        this.token = token;
      } else {
        this.isErrorConnection = true;
      }
    });
  }

  editUser() {
    if (this.user.city === '' || this.user.email === '' || this.user.phone === '' || this.user.username === '') {
      this.isSomeFieldEmpty = true;
    } else {
      this.isSomeFieldEmpty = false;
      console.log('Bearer', this.token);
      this.userService.edit(this.user, this.token).subscribe(
      (response) => {
        if (response === 'ERROR_CONNECTION') {
          this.isErrorConnection = true;
          this.isUserEdited = false;
          this.errors.push('Ha ocurrido un error en la conexión, contactese con el Admin.');
        } else if (response === 'USER_UPDATED') {
          this.isErrorConnection = false;
          this.errors = [];
          this.isUserEdited = true;
        }
      },
      err => {
        this.isErrorConnection = true;
        this.isUserEdited = false;
        this.errors.push('Ha ocurrido un error en la conexión, contactese con el Admin.');
      });
    }
  }

  dismiss() {
    this.modalReference.dismiss();
  }

  close() {
    this.userService.getAllUsers(this.token);
    this.modalReference.close();
  }
}
