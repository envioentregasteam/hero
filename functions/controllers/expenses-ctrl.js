
const Expense = require('../models/expense');

getExpenses = async (req, res) => {
    await Expense.find({}, (err, users) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        return res.status(200).json({ success: true, data: users })
    }).catch(err => console.log(err))
}

module.exports = {
    getExpenses
}