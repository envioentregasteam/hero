const express = require('express')

const UserCtrl = require('../controllers/users-ctrl')

const router = express.Router()

router.get('/users', UserCtrl.getUsers)
// router.put('/user/:id', ExpenseCtrl.updateExpense)
// router.delete('/user/:id', ExpenseCtrl.deleteExpense)
// router.get('/user/:id', ExpenseCtrl.getExpenseById)

module.exports = router