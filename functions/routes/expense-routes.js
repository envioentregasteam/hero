const express = require('express')

const ExpenseCtrl = require('../controllers/expenses-ctrl')

const router = express.Router()

router.get('/get', ExpenseCtrl.getExpenses)
// router.put('/user/:id', ExpenseCtrl.updateExpense)
// router.delete('/user/:id', ExpenseCtrl.deleteExpense)
// router.get('/user/:id', ExpenseCtrl.getExpenseById)

module.exports = router