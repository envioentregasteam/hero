const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Expense = new Schema({
    id: { type: String, required: true},
    title: { type: String, required: true},
    text: { type: String, required: true},
    ammount: { type: Number, required: true},
    date: { type: Date, required: true},
})

module.exports = mongoose.model('expenses', Expense)