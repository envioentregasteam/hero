const mongoose = require('mongoose')
const Schema = mongoose.Schema

const User = new Schema({
    email: { type: String, required: true},
    password: { type: String, required: true},
    username: { type: String, required: true},
    phone: { type: Number, required: true},
    city: { type: String, required: true},
    profile: { type: String, required: true},
    created_at: String
})

module.exports = mongoose.model('users', User)