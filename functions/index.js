const express = require('express');
const bodyPaser = require('body-parser');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const app = express();
const db = require('./db')

const userRouter = require('./routes/user-routes')
const expenseRouter = require('./routes/expense-routes')

const ObjectId = mongoose.Types.ObjectId;

// The Cloud Functions for Firebase SDK to create Cloud Functions and set up triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access Firestore.
const admin = require('firebase-admin');
admin.initializeApp();



const userDB = require('./models/user');

app.use(bodyPaser.json());
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

const authorizationAccessToken = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: 'https://dev-59y7pe59.eu.auth0.com/.well-known/jwks.json'
    }),
    audience: 'https://express.api',
    issuer: 'https://dev-59y7pe59.eu.auth0.com/',
    algorithms: ["RS256"]
});

app.use('/api/user', userRouter)
app.use('/api/expense', expenseRouter)

// app.post('/api/new/stadium', (req, res) => {
//     const {stadium} = req.body;
//     stadiumDB.find({name:stadium.name, city:stadium.city}).count().then(resp => {
//         if(resp > 0) {
//             return res.json('ERROR_EXIST');
//         } else {
//             let newStadium = new stadiumDB({
//                 name: stadium.name,
//                 city: stadium.city,
//                 sport: stadium.sport,
//                 created_at: stadium.created_at
//             });
//             newStadium.save().then(item => {
//                 console.log('item guardado', item); 
//             });
//             return res.json('STADIUM_SAVED');
//         }
//     });
// });

// app.post('/api/new/agenda', (req, res) => {
//     const {agenda} = req.body;
//     agendaDB.find({hour:agendaDB.hour}).count().then(resp => {
//         if(resp > 0) {
//             return res.json('ERROR_EXIST');
//         } else {
//             let newAgenda = new agendaDB({
//                 user: agenda.user,
//                 stadium: agenda.stadium,
//                 day: agenda.day,
//                 hour: agenda.hour,
//                 status: agenda.status,
//                 price: parseFloat(agenda.price),
//                 created_at: agenda.created_at
//             });
//             newAgenda.save().then(item => {
//                 console.log('item guardado', item); 
//             });
//             return res.json('AGENDA_SAVED');
//         }
//     });
// });

// app.post('/api/new/award', (req, res) => {
//     const {award} = req.body;
//     let newAward = new awardsDB({
//         name: award.name,
//         description: award.description,
//         created_at: award.created_at
//     });
//     newAward.save().then(item => {
//         console.log('item guardado', item); 
//     });
//     return res.json('AWARD_SAVED');
// });

// app.post('/api/award/asigned', (req, res) => {
//     let award = req.body.award;
//     let created_at = req.body.created_at;
//     userDB.find({_id:req.body.user}).then(resp => {
//         if (resp.length > 0) {
//             let user = resp[0];
//             let usersAwards = new usersAwardsDB({
//                 user: user,
//                 award: award,
//                 created_at
//             });
//             usersAwards.save().then(item => {
//                 console.log('item guardado', item); 
//             }).catch(err => {
//                 return res.json('ERROR_CONNECTION');    
//             });
//             return res.json('USER_AWARD_SAVED');
//         } else {
//             return res.json('USER_NOT_FOUND');
//         }
//     }).catch(err => {
//         return res.json('ERROR_CONNECTION');    
//     })
// });

// app.post('/api/update/agenda', (req, res) => {
//     const status = req.body;
//     console.log(status);
//     agendaDB.findOneAndUpdate({_id:status.agenda}, {status:status.status}).then(resp => {
//         return res.json('AGENDA_UPDATED');
//     });
// });

// app.get('/api/historical/byDate', (req, res) => {
//     let from = req.query.from;
//     let to = req.query.to;
//     agendaDB.aggregate([
//         {
//             $match: {
//                 "day": {$gte: from, $lt: to},
//                 $or: [{status: 'confirmed'}, {status: 'finished'}],
//             },
//         },
//         {$group: {"_id": "$user.username", "userInfo": {$push: "$user"}, "price": {$sum: "$price"}, count: { $sum: 1 }}},
//         {$project : { "user.username": "$_id", _id: 0, "userInfo": 1, "price": 1, "count" : 1}},
//         {$sort: {count: -1}}
//     ]).then((result) => {
//         return res.json(result);
//     });
// });

// app.get('/api/stadiums/sports', (req, res) => {
//     stadiumDB.aggregate([
//         {$group: {"_id": "$sport"}},
//         {$project : { "sport": "$_id", _id: 0}},
//     ]).then((result) => {
//         return res.json(result);
//     });
// });

app.get('/api/stadiums/cities', (req, res) => {
    stadiumDB.aggregate([
        {$group: {"_id": "$city"}},
        {$project : { "city": "$_id", _id: 0}},
    ]).then((result) => {
        return res.json(result);
    });
});

// app.post('/api/searchUsers', (req, res) => {
//     const {user} = req.body;
//     userDB.find({username: { $regex : new RegExp(user, "i") } }).then((resp) => {
//         if (resp) {
//             return res.json(resp);
//         } else {
//             return res.json('NOT_SRESULT');
//         }
//     },
//     err => {
//         return res.json('ERROR_CONNECTION');
//     })
// });

app.listen(8253, () => console.log('Express listening at 8253'));
exports.app = functions.https.onRequest(app);